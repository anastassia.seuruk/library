from django.contrib import admin
from backend.book.models import Book

admin.site.register(Book)
