from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework import viewsets
from backend.book.models import Book
from backend.book.serializers import BookSerializer


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer

    def perform_create(self, serializer):
        serializer.save()
