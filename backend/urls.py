from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from backend.book.views import BookViewSet
from backend.user.views import UserViewSet

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'books', BookViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
]
