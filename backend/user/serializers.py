from rest_framework import serializers
from backend.user.models import User
from backend.book.serializers import BookSerializer


class UserSerializer(serializers.ModelSerializer):
    books = BookSerializer(many=True, read_only=True)

    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'email', 'date_of_birth', 'books']
