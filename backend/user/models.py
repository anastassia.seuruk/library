from django.db import models
from backend.book.models import Book


class User(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    email = models.EmailField()
    date_of_birth = models.DateField()
    books = models.ManyToManyField(Book)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return f'{self.first_name} {self.last_name}'
