from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from backend.user.models import User
from backend.user.serializers import UserSerializer
from backend.book.serializers import BookSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(detail=True, methods=['get'])
    def books(self, request, pk=None):
        user = self.get_object()
        serializer = BookSerializer(user.books.all(), many=True)
        return Response(serializer.data)
